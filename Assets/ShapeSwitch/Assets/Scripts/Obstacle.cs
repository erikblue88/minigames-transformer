﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    public float movementSpeed, reduceSpeedBy;

    private Vector3 nextPos;

	void Start () {
        if (FindObjectOfType<Spawner>().spawnType == 1)     //If the spawnType is 1, then the obstacles will move slower
            movementSpeed -= reduceSpeedBy;
        else if (FindObjectOfType<Spawner>().spawnType == 2)        //If the spawnType is 2, then the obstacles will move even slower
            movementSpeed -= reduceSpeedBy * 3.2f;
    }
	
	void Update () {
        if (!FindObjectOfType<GameManager>().gameIsOver)      //If the game is not over yet
        {
            //Then obstacle moves towards player
            nextPos = transform.position;
            nextPos.z -= movementSpeed * Time.deltaTime;
            transform.position = nextPos;
        }
	}
}
